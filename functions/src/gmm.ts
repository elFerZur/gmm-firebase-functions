import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import * as dateFns from 'date-fns';
import { File, Bucket, DownloadResponse } from '@google-cloud/storage';
import * as mdl from './model';
import * as props from './properties';
import * as func from './common-functions';
import * as err from './error-codes';


export const newGmmRequest = functions.https.onCall(async (data, context) => {
    const requestData:mdl.IGmm_RemoteRequest = data;
    //Condición necesaria: precición espacial
    if (requestData.gmm.spaceResolution>props.METERS_MAX_RADIUS_GMM){
      console.error("### La precición espacial es demasiado baja!!!");
      const response:mdl.IGmm_RemoteResponse = {idResult:err.GMM_PRECISION_TOO_LOW,text:'Precision muy baja'};
      return (response);
    }
    console.log("### newGmmRequest=> Recibidos los datos="+JSON.stringify(requestData));
    // Authentication / user information is automatically added to the request.
    if (context!==null&&context.auth) {
      const uid = context.auth.uid;
      const name = context.auth.token.name || null;
      const email = context.auth.token.email || null;
      console.log("### uid="+uid+" name="+name+" email="+email);
      try{
        const docRef_user = admin.firestore().doc('users/'+uid);
        const snapshot_doc_user = await docRef_user.get();
        if (snapshot_doc_user === null||snapshot_doc_user.exists === false){
          console.error("### Usuario no EXISTE en bbdd:"+uid+","+name+","+email);
          const response:mdl.IGmm_RemoteResponse = {idResult:err.GMM_NOT_EXISTS,text:'Usuario no existe'};
          return (response);
        }else{
          const userDocumentData = snapshot_doc_user.data();
          if (userDocumentData!==undefined){
            const userBBDD:mdl.IUser_BBDD=mdl.getUserObject(userDocumentData);
            //Comprobaciones para los tipos de usuarios
            const userCheckResult = checkUser(userBBDD);
            if (userCheckResult !== null){
              return userCheckResult;
            }
            //Ya tenemos el usuario para poder trabajar con el  
            const gmmNew:mdl.IGmm_request = requestData.gmm;
            const gmmBBDDNew:mdl.IGmm_BBDD = mdl.newGmmBBDD(gmmNew,uid,userBBDD.nick,1,1);
            const docRef_gmm:FirebaseFirestore.DocumentReference = admin.firestore().collection('/gmm').doc();
            const writeResult_gmm = await docRef_gmm.set(gmmBBDDNew);
            if (writeResult_gmm!==undefined&&writeResult_gmm!==null){
              console.log("### Gmm creado satisfactoriamente id="+docRef_gmm.id);
              //Envío por mensajería
              sendMessageTopic(gmmBBDDNew,props.CODE_TOPIC_GMM_WORLD);
              //Actualización de los datos del usuario
              const increment = admin.firestore.FieldValue.increment(1);
              await docRef_user.update({ gmmOwnCont: increment, gmmLastNewDate: admin.firestore.Timestamp.now() });
              if (requestData.gmm.image64!==null&&requestData.gmm.image64!==undefined){
                console.log("### Detectado GMM con imágen tamaño="+requestData.gmm.image64.length);
                const gmmDate:Date = gmmBBDDNew.gmmDateTime.toDate();
                const storage = admin.storage();
                const bucket:Bucket = storage.bucket(props.FIREBASE_BUCKET);
                const fileName:string = docRef_gmm.id+".jpg";
                const filePath:string =gmmDate.getUTCFullYear()+"/"+gmmDate.getUTCMonth()+"/"+gmmDate.getUTCDate()+"/";
                const file:File = bucket.file(filePath+fileName);
                const imageBuffer = Buffer.from(requestData.gmm.image64, 'base64');
                const mimeType = 'image/jpeg';
                file.save(imageBuffer,{ metadata: { contentType: mimeType } }).then(()=>{
                  console.log("### Fichero almacenado corretamente="+filePath);
                },(error)=>{
                  console.error("### Error almacenando foto="+filePath+" Error:"+error,error);
                });
              }
              const response:mdl.IGmm_RemoteResponse = {idResult:1,text:'Gmm creado hora:'+writeResult_gmm.writeTime,idGmm:docRef_gmm.id};
              return (response);
            }else{
              const response:mdl.IGmm_RemoteResponse = {idResult:err.FIRESTORE_WRITE_GENERAL_ERROR,text:'### No fue posible la creación del documento'};
              return (response);
            }
          }else{
            const response:mdl.IGmm_RemoteResponse = {idResult:err.FIRESTORE_READ_GENERAL_ERROR,text:'### No existia info del usuario: undefined'};
            return (response);
          }
        }
      } catch (error){
        console.error("### Error dando de alta GMM:"+error);
        const response:mdl.IGmm_RemoteResponse = { idResult:err.FIRESTORE_WRITE_GENERAL_ERROR,text:error};
        return (response);
      }
    }else{
      const response:mdl.IGmm_RemoteResponse = { idResult:err.USER_NOT_LOGGED,text:'### Usuario no logado en Firebase'} ;
      return (response);
    }
});

export const getGmmImageRequest = functions.https.onCall(async (data, context) => {
  const idGmm:string = data.idGmm;
  console.log("### getGmmImageRequest=> Recibida petición para imagen de GMM="+idGmm);
  // Authentication / user information is automatically added to the request.
  if (context!==null&&context.auth) {
    const uid = context.auth.uid;
    const name = context.auth.token.name || null;
    const email = context.auth.token.email || null;
    console.log("### uid="+uid+" name="+name+" email="+email);
    try{
      const docRef_gmm = admin.firestore().doc('gmm/'+idGmm);
      const snapshot_doc_gmm = await docRef_gmm.get();
      if (snapshot_doc_gmm === null||snapshot_doc_gmm.exists === false){
        console.error("### No existe el GMM o no es del usuario bbdd");
        const response:mdl.IGmm_RemoteResponse = {idResult:err.FIRESTORE_READ_GENERAL_ERROR,text:'No existe GMM'};
        return (response);
      }else{
        const gmmDocumentData = snapshot_doc_gmm.data();
        if (gmmDocumentData !== null && gmmDocumentData !== undefined && gmmDocumentData.idUser === uid){
          //El GMM es del usuario que realiza la petición
          const gmm:mdl.IGmm_BBDD = mdl.getGmmObject(gmmDocumentData);
          const storage = admin.storage();
          const bucket:Bucket = storage.bucket(props.FIREBASE_BUCKET);
          const filePath:string =
            gmm.gmmDateTime.toDate().getUTCFullYear()+"/"+
            gmm.gmmDateTime.toDate().getUTCMonth()+"/"+
            gmm.gmmDateTime.toDate().getUTCDate()+"/"+
            idGmm+".jpg";
          const file:File = bucket.file(filePath);
          if (file.exists()){
            try {
              const downloadResponse:DownloadResponse = await file.download();
              console.log("### Recuperando buffer de la respuesta longitud="+downloadResponse.length);
              const bufferImage = downloadResponse.pop();
              if (bufferImage !== undefined){
                const response:mdl.IGmmImage_RemoteResponse = {idResult:1,image64:bufferImage.toString("base64")};
                return (response);
              }else{
                const response:mdl.IGmmImage_RemoteResponse = {idResult:106,text:'Error general recuperando imagen'};
                return (response);
              }
            } catch (error){
              console.error("### Error recuperando fichero:",error);
              const response:mdl.IGmmImage_RemoteResponse = {idResult:err.CLOUD_STORAGE_GENERAL_ERRROR,text:'Error general'};
              return (response);
            }
          }else{
            console.error("### Error fichero no existente");
            const response:mdl.IGmmImage_RemoteResponse = {idResult:err.CLOUD_STORAGE_FILE_NOT_EXISTS,text:'No existe fichero'};
            return (response);
          }
        }else{
          console.error("### Error el usuario no coincide");
          const response:mdl.IGmmImage_RemoteResponse = {idResult:err.CLOUD_STORAGE_FILE_NOT_YOURS,text:'No acceso a GMM no propios'};
          return (response);
        }
      }
    } catch (error){
      console.error("### Error general accediendo a la imagen",error);
      const response:mdl.IGmmImage_RemoteResponse = {idResult:err.CLOUD_STORAGE_GENERAL_ERRROR,text:'Error general'};
      return (response);
    }
  }else{
    console.error("### Acceso no permitido sin usuario ###");
    const response:mdl.IGmmImage_RemoteResponse = {idResult:err.USER_NOT_LOGGED,text:'Sin usuario'};
    return (response);
  }
});

export const updateGmmRequest = functions.https.onCall(async (data, context) => {
  const requestData:mdl.IGmm_Update_RemoteRequest = data;
  console.log("### updateGmmRequest=> Recibidos los datos="+JSON.stringify(requestData));
  // Authentication / user information is automatically added to the request.
  if (context!==null&&context.auth) {
    const uid = context.auth.uid;
    const name = context.auth.token.name || null;
    const email = context.auth.token.email || null;
    console.log("### uid="+uid+" name="+name+" email="+email);
    try{
      const docRef_user = admin.firestore().doc('users/'+uid);
      if (docRef_user === null){
        console.error("### Usuario no EXISTE en bbdd:"+uid);
        const response:mdl.IGmm_RemoteResponse = {idResult:err.USER_NOT_EXISTS,text:'Usuario no existe'};
        return (response);
      }
      const snapshot_doc_user = await docRef_user.get();
      if (snapshot_doc_user === null||snapshot_doc_user.exists === false){
        console.error("### Usuario no EXISTE en bbdd"+uid);
        const response:mdl.IGmm_RemoteResponse = {idResult:err.FIRESTORE_READ_GENERAL_ERROR,text:'Usuario no existe'};
        return (response);
      }else{
        const userDocumentData = snapshot_doc_user.data();
        if (userDocumentData !== undefined){
          const docRef_gmm = admin.firestore().doc('gmm/'+requestData.idGmm);
          if (docRef_gmm === null){
            console.error("### El GMM no existe:"+requestData.idGmm);
            const response:mdl.IGmm_RemoteResponse = {idResult:err.GMM_NOT_EXISTS,text:'GMM no existe'};
            return (response);
          }
          const snapshot_doc_gmm = await docRef_gmm.get();
          if (snapshot_doc_gmm === null||snapshot_doc_gmm.exists === false){
            console.error("### No existe el GMM o no es del usuario bbdd");
            const response:mdl.IGmm_RemoteResponse = {idResult:err.FIRESTORE_READ_GENERAL_ERROR,text:'No existe GMM'};
            return (response);
          }else{
            const gmmDocumentData = snapshot_doc_gmm.data();
            if (gmmDocumentData !== undefined){
              const gmmBBDD:mdl.IGmm_BBDD=mdl.getGmmObject(gmmDocumentData);
              if (uid !== gmmBBDD.idUser){
                console.error("### El GMM no era tuyo bandarra!!");
                const response:mdl.IGmm_RemoteResponse = {idResult:err.GMM_NOT_YOURS,text:'GMM no era tuyo'};
                return (response);
              }else{
                //Antes de actualizar comprobamos la restricción de la fecha
                const ahora:Date = admin.firestore.Timestamp.now().toDate();
                const gmmDate:Date = gmmBBDD.gmmDateTime.toDate();
                const days:number=dateFns.differenceInDays(ahora,gmmDate);
                if (days>props.DAYS_MAX_GMM_EDIT){
                  console.warn("### Se han superado el límite para actalizar GMM!!");
                  const response:mdl.IGmm_RemoteResponse = {idResult:err.GMM_LIMIT_TIME_UPDATE_REACHED,text:'Superado tiempo máximo para actualizar'};
                  return (response);
                }
                //Procedemos a realizar el update
                const writeResult_gmm = await docRef_gmm.update(requestData.gmmUpdate);
                if (writeResult_gmm !== undefined&&writeResult_gmm !== null){
                  console.log("### Resultado de actualizar GMM="+requestData.idGmm+":"+writeResult_gmm.writeTime);
                  //Llegados aquí ya está actualizado el GMM
                  if (!requestData.gmmUpdate.isImage && gmmBBDD.isImage){
                    console.log("### Procedemos a borrar foto del GMM="+requestData.idGmm);
                    deletePhoto(requestData.idGmm,gmmBBDD.gmmDateTime);
                  }
                  console.info("### GMM actualizado ###");
                  const response:mdl.IGmm_RemoteResponse = {idResult:1,text:'GMM actualizado'};
                  return (response);
                }else{
                  //Error actualizando
                  console.error("### Error realizando la actualización GMM:"+requestData.idGmm);
                  const response:mdl.IGmm_RemoteResponse = {idResult:err.FIRESTORE_UPDATING_GENERAL_ERROR,text:'Error actualizando GMM'};
                  return (response);
                }
              }
            }else{
              //No ha sido posible la lectura de bb.dd
              console.error("### El GMM no existía en BBDD");
              const response:mdl.IGmm_RemoteResponse = {idResult:err.FIRESTORE_READ_GENERAL_ERROR,text:'Error actualizando GMM'};
              return (response);
            }
          }
        }else{
          const response:mdl.IGmm_RemoteResponse = {idResult:err.FIRESTORE_READ_GENERAL_ERROR,text:'### No existia info del usuario: undefined'};
          return (response);
        }
      }
    } catch (error){
      console.error("### Error general actualizando GMM:",error);
      const response:mdl.IGmmImage_RemoteResponse = {idResult:err.FIRESTORE_UPDATING_GENERAL_ERROR,text:'Error general'};
      return (response);
    }
  }else{
    console.error("### Acceso no permitido sin usuario ###");
    const response:mdl.IGmmImage_RemoteResponse = {idResult:err.USER_NOT_LOGGED,text:'Sin usuario'};
    return (response);
  }
});

function deletePhoto(idGmm:string,dateGmm:admin.firestore.Timestamp){
  try{
    const storage = admin.storage();
    const bucket:Bucket = storage.bucket(props.FIREBASE_BUCKET);
    const year = dateGmm.toDate().getUTCFullYear();
    const month = dateGmm.toDate().getUTCMonth();
    const day = dateGmm.toDate().getUTCDate();
    const filePath:string = year+"/"+month+"/"+day+"/"+idGmm+".jpg";
    const file:File = bucket.file(filePath);
    file.exists().then((result)=>{
      if (result[0]){
        file.delete().
        then((result2)=>console.log("### Resultado borrado foto="+result2)).
        catch(error=>console.error("### Error borrando GMM:"+filePath));
      }else{
        console.error("### No existía el fichero para borrar="+filePath);
      }
    },(error)=>{
      console.error("### Error comprobando si existe fichero:"+error);
    });
  } catch (error){
    console.error("### Error general borrando foto:"+idGmm);
  }
}

export const deleteGmmRequest = functions.https.onCall(async (data, context) => {
  const requestData:mdl.IGmm_Delete_RemoteRequest = data;
  console.log("### deleteGmmRequest=> Recibidos los datos="+JSON.stringify(requestData));
  // Authentication / user information is automatically added to the request.
  if (context!==null&&context.auth) {
    const uid = context.auth.uid;
    const name = context.auth.token.name || null;
    const email = context.auth.token.email || null;
    console.log("### uid="+uid+" name="+name+" email="+email);
    try{
      const docRef_user = admin.firestore().doc('users/'+uid);
      if (docRef_user === null){
        console.error("### Usuario no EXISTE en bbdd:"+uid);
        const response:mdl.IGmm_RemoteResponse = {idResult:err.USER_NOT_EXISTS,text:'Usuario no existe'};
        return (response);
      }
      const docRef_gmm = admin.firestore().doc('gmm/'+requestData.idGmm);
      if (docRef_gmm === null){
        console.error("### El GMM no existe:"+requestData.idGmm);
        const response:mdl.IGmm_RemoteResponse = {idResult:err.GMM_NOT_EXISTS,text:'GMM no existe'};
        return (response);
      }
      const snapshot_doc_gmm = await docRef_gmm.get();
      if (snapshot_doc_gmm !==null && snapshot_doc_gmm.exists){
        const gmmDocumentData = snapshot_doc_gmm.data();
        if (gmmDocumentData !== undefined){
          const gmmBBDD:mdl.IGmm_BBDD=mdl.getGmmObject(gmmDocumentData);
          if (uid !== gmmBBDD.idUser){
            console.error("### El GMM no era tuyo bandarra!!");
            const response:mdl.IGmm_RemoteResponse = {idResult:err.GMM_NOT_YOURS,text:'GMM no era tuyo'};
            return (response);
          }else{
            //Tendremos que borrar varias cosas: 
            //1.- Los comentarios que cuelgan en una subcolección
            const resultDeleteComments = func.deleteCollection("gmm/"+requestData.idGmm+"/comments");
            console.log("### Resultado borrado de comentarios="+resultDeleteComments);
            //2.- Eliminación del GMM en sí
            const resultDelete = await docRef_gmm.delete();
            if (resultDelete!==null&&resultDelete.writeTime!==null){
              //Borrado correcto
              //Actualización de los datos del usuario
              const decrement = admin.firestore.FieldValue.increment(-1);
              await docRef_user.update({ gmmOwnCont: decrement, gmmLastNewDate: admin.firestore.Timestamp.now() });
              //Borrado de la foto si la hubiera
              if (gmmBBDD.isImage){
                console.log("### Detectado GMM a borrar con imagen");
                deletePhoto(requestData.idGmm,gmmBBDD.gmmDateTime);
              }
              const response:mdl.IGmm_RemoteResponse = {idResult:1,text:'Borrado correcto del GMM'};
              return (response);  
            }else{
              const response:mdl.IGmm_RemoteResponse = {idResult:err.FIRESTORE_DELETE_GENERAL_ERROR,text:'Borrado no correcto del GMM'};
              return (response); 
            }
          }
        }else{
          //No ha sido posible la lectura de bb.dd
          console.error("### El GMM no existía en BBDD");
          const response:mdl.IGmm_RemoteResponse = {idResult:err.FIRESTORE_READ_GENERAL_ERROR,text:'Error actualizando GMM'};
          return (response);
        }
      }else{
        console.error("### GMM no EXISTE en bbdd");
        const response:mdl.IGmm_RemoteResponse = {idResult:err.GMM_NOT_EXISTS,text:'GMM no existe'};
        return (response); 
      }
    } catch (error){
      console.error("### Error general actualizando GMM:",error);
      const response:mdl.IGmmImage_RemoteResponse = {idResult:err.FIRESTORE_UPDATING_GENERAL_ERROR,text:'Error general'};
      return (response);
    }
  }else{
    console.error("### Acceso no permitido sin usuario ###");
    const response:mdl.IGmmImage_RemoteResponse = {idResult:err.USER_NOT_LOGGED,text:'Sin usuario'};
    return (response);
  }
});

function checkUser(userBBDD:mdl.IUser_BBDD):mdl.IGmm_RemoteResponse|null{
  let response:mdl.IGmm_RemoteResponse;
  if (userBBDD.userType === props.CODE_USERTYPE_BLOCK_FAKE_GPS){
    console.warn("### OJO, detectado usuario bloqueado FAKE GPS intentando alta GMM");
    response = {idResult:err.USER_BLOCKED_FAKE_GPS,text:'Estas bloqueado por FAKE GPS'};
    return (response);
  } else if (userBBDD.userType === props.CODE_USERTYPE_BLOCK_MAI){
    console.warn("### OJO, detectado usuario bloqueado MAI intentando alta GMM");
    response = {idResult:err.USER_BLOCKED_MAI,text:'Estas bloqueado por MAI'};
    return (response);
  } else if (userBBDD.userType === props.CODE_USERTYPE_ADMIN_SUPER){
    //SuperUsuario code 100: sin limites
    return null;
  } else if (userBBDD.userType < props.CODE_USERTYPE_ADMIN_SUPER){
    //Usuarios normales: de 0 a 99
    if (userBBDD.gmmLastNewDate!==null&&userBBDD.gmmLastNewDate!==undefined){
      const ahora:Date = admin.firestore.Timestamp.now().toDate();
      const gmmDate:Date = userBBDD.gmmLastNewDate.toDate();
      const hours:number=dateFns.differenceInHours(ahora,gmmDate);
      if (hours<userBBDD.gmmHoursBetween){
        console.warn("### OJO, han pasado solo "+hours+" horas");
        response = {idResult:err.GMM_CREATE_TOO_FAST,text:'Tienes que esperar un poco'};
        return (response);
      }
    }
  }
  return null;
}

export const newMaiGmmRequest = functions.https.onCall(async (data, context) => {
  const requestData:mdl.IGmmMai_request = data;
  console.log("### newMaiGmmRequest=> Recibidos los datos="+JSON.stringify(requestData));
  // Authentication / user information is automatically added to the request.
  if (context===null||context.auth===null||context.auth===undefined){
    const response:mdl.IGmmCmmMai_response = { idResult:err.USER_NOT_LOGGED,text:'### Usuario no logado en Firebase'} ;
    return (response);
  }
  const uid = context.auth.uid;
  //Comprobaciones para poder realizar el MAI en ese comentario
  try{
    //Primero recuperamos el usuario
    const docRef_user = admin.firestore().doc('users/'+uid);
    const snapshot_doc_user = await docRef_user.get();
    if (snapshot_doc_user === null || snapshot_doc_user.exists === false){
      console.error("### Usuario no EXISTE en bbdd");
      const response:mdl.IGmmMai_response = {idResult:err.USER_NOT_EXISTS,text:'Usuario no existe'};
      return (response);
    }
    //Comprobamos si el usuario ya ha hecho el MAI
    const docRef_userMaiGmm:FirebaseFirestore.DocumentReference = 
      admin.firestore().collection('/users/'+uid+'/maiOtherGmm').doc(requestData.idGmm);
    if (docRef_userMaiGmm!==null){
      const snapshot_doc_userMaiGmm = await docRef_userMaiGmm.get();
      if (snapshot_doc_userMaiGmm!==null&&snapshot_doc_userMaiGmm.exists){
        //Ojo, que el usuario ya había marcado este comentario como MAI
        const maiGmm:mdl.IMaiGmm_BBDD =  mdl.getMaiGmmObject(snapshot_doc_userMaiGmm);
        const response:mdl.IGmmMai_response = {
          idResult:err.GMM_MAI_EXISTS,
          msDataTime: maiGmm.msInsertDateTime,
          text:'El usuario ya había marcado como inapropiado'};
        return (response);
      }
    }
    //A partir de aqui, el usuario no había registrado el MAI
    const maiGmmBBDD:mdl.IMaiGmm_BBDD = mdl.newMaiGmmBBDD(admin.firestore.Timestamp.now());
    //Guardamos el documento
    await docRef_userMaiGmm.set(maiGmmBBDD);
    //Actualizamos contador de MAI del GMM
    const docRef_Gmm = admin.firestore().doc('/gmm/'+requestData.idGmm);
    const increment = admin.firestore.FieldValue.increment(1);
    await docRef_Gmm.update({ maiTimes: increment });
    //Y el contador de mai de comentarios del usuario
    await docRef_user.update({ maiOtherGmmCont: increment });
    const responseOK:mdl.IGmmMai_response = { idResult:1,text:'### Creado MAI correctamente!!!'} ;
    return (responseOK);
  } catch (error){
      console.error("### Error General:"+error);
      const response:mdl.IGmmMai_response = { idResult:err.FIRESTORE_GENERAL_ERROR,text:'### Error general en Firebase'} ;
      return (response);
  }
});


function sendMessageTopic(gmm:mdl.IGmm_BBDD,topicName:string){
  let textDesc:string = gmm.textDesc!==undefined?gmm.textDesc:"";
  let textDescLang:string = gmm.textDescLang!==undefined?gmm.textDescLang:"";
  let nick:string = gmm.nick!==undefined?gmm.nick:"";
  if (gmm.isAnonimus){
    textDesc = "";
    textDescLang = "";
    nick = "";
  }
  const message = {
    data: {
      nick: nick,
      longitud: gmm.longitud.toFixed(6),
      latitud: gmm.latitud.toFixed(6),
      typeEmotion: gmm.typeEmotion.toString(),
      levelEmotion: gmm.levelEmotion.toString(),
      typeSocialEnvironment: gmm.typeSocialEnvironment.toString(),
      spaceResolution: gmm.spaceResolution.toFixed(0),
      msDateTime: gmm.msDateTime.toString(),
      textDesc: textDesc,
      textDescLang: textDescLang,
      isAnonimus: gmm.isAnonimus?'true':'false'
    }
  };
  admin.messaging().sendToTopic(topicName,message).then((topicResponse:admin.messaging.MessagingTopicResponse)=>{
    console.log("### Resultado de envío="+topicResponse.messageId);
  }).catch(error=>{
    console.error("### Error enviando mensaje a TOPIC:"+topicName+" Error:"+error);
  });
}