import * as functions from 'firebase-functions';
import { messaging } from 'firebase-admin';

export const sendFCMTest = functions.https.onRequest((request, response) => {
    const message = {
        data: {
          score: '850',
          time: '2:45'
        }
      };
    messaging().sendToTopic("test01",message)
        .then((topicResponse:messaging.MessagingTopicResponse)=>{
            console.log("### Resultado de envío="+topicResponse.messageId);
        }
        ).catch(error=>console.error("### Error:"+error));
    response.send("Hello from Firebase GMM sending to topic request="+request);
  });