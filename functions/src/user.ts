import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import { IUser_BBDD, IUser_RemoteRequest, IUser_RemoteResponse } from './model';
import * as err from './error-codes';

export const userRequest = functions.https.onCall((data, context) => {
    const requestData:IUser_RemoteRequest = data;
    console.log("### newUserRequest=> Recibidos los datos="+JSON.stringify(requestData));
    // Authentication / user information is automatically added to the request.
    if (context!==null&&context.auth) {
      const uid = context.auth.uid;
      const name = context.auth.token.name || null;
      const email = context.auth.token.email || null;
      console.log("### uid="+uid+" name="+name+" email="+email);
      return new Promise((resolve, reject) => {
        //Consulta a la coleccion de usuarios
        admin.firestore().doc('users/'+uid).get().then(
          (document)=>{
          if (document===null||document.exists === false){
            console.log("### Usuario no EXISTE en bbdd");
            if (requestData.type === 'newUser'){
              console.log("### Request type = newUser ###");
              //Comprobación de nick único
              if (requestData.nick===null||requestData.nick.length<=1||requestData.nick.length>10){
                resolve({'idResult':err.USER_NICK_LENGTH_NOK});
              }
              const users = admin.firestore().collection('users');
              users.where('nick','==',requestData.nick).limit(1).get().then(
                (querySnapshot)=>{
                    if (querySnapshot.docs.length>=1){
                      console.warn("### Ya existe un usuario registrado con ese nick="+requestData.nick);
                      resolve({'idResult':err.USER_NICK_EXISTS});
                    }else{
                      console.log("### Nick correcto: no existe en bb.dd");
                      //Proceso de creación de usuario
                      createUser(uid,requestData.nick,email,requestData.language,requestData.fcmToken).then(
                        (result)=>{
                          if (result!==null){
                            const userResponse:IUser_RemoteResponse = { idResult:1,user:result };
                            console.log("### Resultado de creación de usuario="+result);
                            resolve(userResponse);
                          } else {
                            resolve({'idResult':err.FIRESTORE_WRITE_GENERAL_ERROR});
                          }
                        },(error)=>{
                          console.error("### Resultado de creación de usuario="+error);
                          resolve({'idResult':err.FIRESTORE_GENERAL_ERROR});
                        });
                    }
                },(error)=>{
                    console.error("### Error accediendo a ficheros:"+error);
                    resolve({'idResult':err.FIRESTORE_READ_GENERAL_ERROR});
              });
            }else{
              console.info("### Petición de usuario que no existe todavía ###");
              resolve({'idResult':err.USER_NOT_EXISTS});
            }
          }
          if (document!==null&&document.exists === true){
            console.log("### Usuario EXISTE en bbdd");
            if (requestData.type === 'getUser'){
              console.log("### Request type = getUser ###");
              resolve({'idResult':2,'user':document.data()});
            }else if (requestData.type === 'updateNick'){
              const users = admin.firestore().collection('users');
              users.where('nick','==',requestData.nick).limit(1).get().then((querySnapshot)=>{
                  if (querySnapshot.docs.length>=1){
                    console.warn("### Ya existe un usuario registrado con ese nick="+requestData.nick);
                    resolve({'idResult':err.USER_NICK_EXISTS});
                  }else{
                    //Cambiamos el nick del usuario
                    const docRef_user = admin.firestore().doc('users/'+uid);
                    docRef_user.update({ nick: requestData.nick }).then(result=>{
                      resolve({'idResult':1});
                    },error=>{
                      resolve({'idResult':err.FIRESTORE_UPDATING_GENERAL_ERROR});
                    });
                  }
                },(error)=>{
                  resolve({'idResult':err.FIRESTORE_READ_GENERAL_ERROR});
                }
              );
            }else if (requestData.type === 'updateFcmToken'){
              //Cambiamos el token
              const docRef_user = admin.firestore().doc('users/'+uid);
              docRef_user.update({ fcmToken: requestData.fcmToken }).then(result=>{
                resolve({'idResult':1});
              },error=>{
                resolve({'idResult':err.FIRESTORE_UPDATING_GENERAL_ERROR});
              });
            }else{
              console.error("### No contemplado: existe usuario y newUser");
              resolve({'idResult':err.FIRESTORE_GENERAL_ERROR});
            }
          }
        },(error)=>{
            console.error("### Error: no ha sido posible ejecutar query usuarios:",error);
            resolve({'idResult':err.FIRESTORE_READ_GENERAL_ERROR});
        }).catch(error=>{
            console.error("### Excepcion:"+error);
            resolve({'idResult':err.FIRESTORE_GENERAL_ERROR});
        });
      });
    }else{
      console.warn("### Sin información de autentificación de firebase ###");
      return ({'idResult':err.USER_NOT_LOGGED});
    }
  });



  function createUser(uid:string,nick:string,email:string|null,language:string,fcmToken?:string):Promise<IUser_BBDD>{
    const user:IUser_BBDD={
      nick: nick,
      email:email!==null?email:"",
      userType: 1,
      language: language,
      dateRegistered: admin.firestore.Timestamp.now(),
      dateLastAccess: admin.firestore.Timestamp.now(),
      gmmOwnCont: 0,
      gmmPreCont: 0,
      gmmOnlineResolvedCont: 0,
      gmmHoursBetween: 6,
      gmmOtherComments: 0,
      fcmToken: fcmToken
    };
    return new Promise<IUser_BBDD>((resolve, reject) => {
      admin.firestore().collection('/users').doc(uid).set(user).then(
        (result)=>{
            console.log("### Documento creado O.K en hora:"+result.writeTime);
            resolve(user);
        },(error)=>{
            console.error("### Error accediendo a doc usuario:"+error);
            reject();
        });
    });
  }