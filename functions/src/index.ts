import * as admin from "firebase-admin";
admin.initializeApp();

export { userRequest } from './user';
export { newGmmRequest, getGmmImageRequest, updateGmmRequest, deleteGmmRequest,newMaiGmmRequest } from './gmm';
export { newGmmCmmRequest, updateGmmCmmRequest, deleteGmmCmmRequest, newMaiCmmRequest } from './gmm-comment';
export { queryGmmRequestTime, queryGmmRequestGeom, queryGmmRequestGeomTime, queryGmmOnlineTime } from './gmm-query';
export { sendFCMTest } from './fcm-test';
  

 