export const FIREBASE_BUCKET:string = "geomoments-f9aaf.appspot.com";
export const DAYS_MAX_GMM_EDIT:number = 5;
export const METERS_MAX_GMM_COMMENT:number = 5000;
export const MINUTES_MAX_GMM_COMMENT:number = 30;
export const METERS_MAX_RADIUS_GMM:number = 5000;
export const MINUTES_QUERY_GMM:number = 100;
export const MINUTES_QUERY_ONLINE_GMM:number = 24*60;
export const HOURS_QUERY_GMM_GEOM_TIME:number = 24;
export const SIZE_QUERY_GMM:number = 100;
export const SIZE_QUERY_GEOM_GMM:number = 100;
export const SIZE_QUERY_TIME_GMM:number = 100;
export const DAYS_MAX_QUERY_TIME_GMM:number = 30;
//GEOHASH: 
// 1 	≤ 5,000km 	× 	5,000km
// 2 	≤ 1,250km 	× 	625km
// 3 	≤ 156km 	× 	156km
// 4 	≤ 39.1km 	× 	19.5km
// 5 	≤ 4.89km 	× 	4.89km
// 6 	≤ 1.22km 	× 	0.61km
// 7 	≤ 153m 	    × 	153m
// 8 	≤ 38.2m 	× 	19.1m
// 9 	≤ 4.77m 	× 	4.77m
// 10 	≤ 1.19m 	× 	0.596m
// 11 	≤ 149mm 	× 	149mm
// 12 	≤ 37.2mm 	× 	18.6mm
export const SIZE_GEOHASH_QUERY = 5;
export const COLUMN_GEOHASH = "geohash5";
export const COLUMN_GEOHASH_QUERY_GEOM = "geohash5";
//Codigos de tipos de usuarios
export const CODE_USERTYPE_BLOCK_FAKE_GPS = 1001;
export const CODE_USERTYPE_BLOCK_MAI = 1002;
export const CODE_USERTYPE_ADMIN_SUPER = 100;
export const CODE_USERTYPE_ADMIN_COUNTRY = 50;
//Código de TOPICS de mensajería FIREBASE
export const CODE_TOPIC_GMM_WORLD = 'gmmWorld'
