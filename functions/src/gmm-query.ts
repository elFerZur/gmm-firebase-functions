import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import * as dateFns from 'date-fns';
import * as mdl from './model';
import * as props from './properties';
import * as geohash from 'latlon-geohash';
import * as err from './error-codes';


export const queryGmmRequestTime = functions.https.onCall(async (data, context) => {
    const requestData:mdl.IGmmQuery_request = data;
    console.log("### queryGmmRequest=> Recibidos los datos="+JSON.stringify(requestData));
    // Authentication / user information is automatically added to the request.
    if (context===null||context.auth===null||context.auth===undefined){
        console.error("### Usuario no EXISTE en bbdd");
        const response1:mdl.IGmmQuery_RemoteResponse = {
            idResult:err.USER_NOT_LOGGED,
            text:'Usuario no logado',
            size:0
        };
        return (response1);
    }
    //Comprobación parámetros de entrada
    if (requestData.currentCountAfter!==undefined){
        if (requestData.currentCountAfter>props.SIZE_QUERY_TIME_GMM){
            console.error("### Parámetro incorrecto!:"+requestData.currentCountAfter);
            const response8:mdl.IGmmQuery_RemoteResponse = {
                idResult:err.GMM_QUERY_PARAM_ERROR,
                text:'Error en parámetro de entrada a la query',
                size:0
            };
        return (response8);
        }
    }
    const uid = context.auth.uid;
    //Comprobaciones para poder realizar el comentario en el GMM
    try{
        //Primero recuperamos el usuario
        const docRef_user = admin.firestore().doc('users/'+uid);
        const snapshot_doc_user = await docRef_user.get();
        if (snapshot_doc_user===null||snapshot_doc_user.exists === false){
          console.error("### Usuario no EXISTE en bbdd");
          const response2:mdl.IGmmQuery_RemoteResponse = {
            idResult:err.USER_NOT_EXISTS,
            text:'Usuario no existe',
            size:0
          };
          return (response2);
        }
        const userDocumentData = snapshot_doc_user.data();
        if (userDocumentData===null||userDocumentData===undefined){
          console.error("### Usuario no EXISTE en bbdd");
          const response3:mdl.IGmmQuery_RemoteResponse = {
            idResult:err.FIRESTORE_READ_GENERAL_ERROR,
            text:'Usuario no existe',
            size:0
          };
          return (response3);
        }
        //const userBBDD:mdl.IUser_BBDD=mdl.getUserObject(userDocumentData);
        //Ahora recuperamos el GMM
        const docRef_gmm = admin.firestore().doc('gmm/'+requestData.idGmmFrom);
        const snapshot_doc_gmm = await docRef_gmm.get();
        if (snapshot_doc_gmm===null||snapshot_doc_gmm.exists === false){
            console.error("### No existe el GMM");
            const response4:mdl.IGmmQuery_RemoteResponse = {
                idResult:err.GMM_NOT_EXISTS,
                text:'No existe GMM',
                size:0
            };
            return (response4);
        }
        const gmmDocumentData = snapshot_doc_gmm.data();
        if (gmmDocumentData===null||gmmDocumentData===undefined){
            console.error("### No existe el GMM");
            const response5:mdl.IGmmQuery_RemoteResponse = {
                idResult:err.FIRESTORE_READ_GENERAL_ERROR,
                text:'No existe GMM',
                size:0
            };
            return (response5);
        }
        const gmm:mdl.IGmm_BBDD = mdl.getGmmObject(gmmDocumentData);
        if (gmm.idUser !== uid){
            console.error("### El GMM no era del usuairo");
            const response6:mdl.IGmmQuery_RemoteResponse = {
                idResult:err.GMM_NOT_YOURS,
                text:'No existe GMM',
                size:0
            };
            return (response6);
        }
        //Ya tenemos el objeto de base de datos con el GMM a partir del cual realizaremos la búsqueda
        //Montamos la consulta
        const firestoreCollectionRef = admin.firestore().collection("/gmm");
        //Límites de tiempo
        const gmmDateLimitBefore:Date = dateFns.subDays(gmm.gmmDateTime.toDate(),props.DAYS_MAX_QUERY_TIME_GMM);
        const gmmDateLimitAfter:Date = dateFns.addDays(gmm.gmmDateTime.toDate(),props.DAYS_MAX_QUERY_TIME_GMM);
        let queryGmmResultAfter;
        let queryGmmResultBefore;
        if (requestData.fromDateTimeMs===null||requestData.fromDateTimeMs===undefined){
            queryGmmResultBefore = await firestoreCollectionRef.
                where('gmmDateTime','<',gmm.gmmDateTime).
                where('gmmDateTime','>',gmmDateLimitBefore).
                orderBy('gmmDateTime','desc').
                limit(props.SIZE_QUERY_TIME_GMM).
                get();
            queryGmmResultAfter = await firestoreCollectionRef.
                where('gmmDateTime','>',gmm.gmmDateTime).
                where('gmmDateTime','<',gmmDateLimitAfter).
                orderBy('gmmDateTime','asc').
                limit(props.SIZE_QUERY_TIME_GMM).
                get();
        }else{
            queryGmmResultAfter = await firestoreCollectionRef.
                where('gmmDateTime','>',admin.firestore.Timestamp.fromMillis(requestData.fromDateTimeMs)).
                orderBy('gmmDateTime','asc').
                limit(requestData.currentCountAfter!==undefined
                    ?props.SIZE_QUERY_TIME_GMM-requestData.currentCountAfter
                    :props.SIZE_QUERY_TIME_GMM).
                get();
        }
        //Procesamos los datos para devolverlos(los anónimos hay que quitar información)
        const returnGmmArray:mdl.IGmm_ForQueries [] = processPrivateGmm(queryGmmResultAfter,uid);
        if (queryGmmResultBefore!==undefined){
            returnGmmArray.push(...processPrivateGmm(queryGmmResultBefore,uid));
        }
        console.info("### Devolviendo GMMS tamaño="+returnGmmArray.length);
        const responseOK:mdl.IGmmQuery_RemoteResponse = {
            idResult:1,
            text:'Consulta correcta',
            size:returnGmmArray.length,
            gmms:returnGmmArray
        };
        return (responseOK);
    } catch (error){
        console.error("### Error General:"+error);
        const response7:mdl.IGmmQuery_RemoteResponse = {
            idResult:err.FIRESTORE_GENERAL_ERROR,
            text:'Error general',
            size:0
        };
        return (response7);
    }
});


export const queryGmmRequestGeom = functions.https.onCall(async (data, context) => {
    const requestData:mdl.IGmmQuery_request = data;
    console.log("### queryGmmRequestGeom=> Recibidos los datos="+JSON.stringify(requestData));
    // Authentication / user information is automatically added to the request.
    if (context===null||context.auth===null||context.auth===undefined){
        console.error("### Usuario no EXISTE en bbdd");
        const response1:mdl.IGmmQuery_RemoteResponse = {
            idResult:err.USER_NOT_LOGGED,
            text:'Usuario no logado',
            size:0
        };
        return (response1);
    }
    const uid = context.auth.uid;
    //Comprobación parámetros de entrada
    if (requestData.currentCountAfter!==undefined){
        if (requestData.currentCountAfter>props.SIZE_QUERY_GEOM_GMM){
            console.error("### Parámetro incorrecto!:"+requestData.currentCountAfter);
            const response8:mdl.IGmmQuery_RemoteResponse = {
                idResult:err.GMM_QUERY_PARAM_ERROR,
                text:'Error en parámetro de entrada a la query',
                size:0
            };
        return (response8);
        }
    }
    //Comprobaciones para poder realizar el comentario en el GMM
    try{
        //Primero recuperamos el usuario
        const docRef_user = admin.firestore().doc('users/'+uid);
        const snapshot_doc_user = await docRef_user.get();
        if (snapshot_doc_user===null||snapshot_doc_user.exists === false){
          console.error("### Usuario no EXISTE en bbdd");
          const response2:mdl.IGmmQuery_RemoteResponse = {
            idResult:err.USER_NOT_EXISTS,
            text:'Usuario no existe',
            size:0
          };
          return (response2);
        }
        const userDocumentData = snapshot_doc_user.data();
        if (userDocumentData===null||userDocumentData===undefined){
          console.error("### Usuario no EXISTE en bbdd");
          const response3:mdl.IGmmQuery_RemoteResponse = {
            idResult:err.FIRESTORE_READ_GENERAL_ERROR,
            text:'Usuario no existe',
            size:0
          };
          return (response3);
        }
        //const userBBDD:mdl.IUser_BBDD=mdl.getUserObject(userDocumentData);
        //Ahora recuperamos el GMM
        const docRef_gmm = admin.firestore().doc('gmm/'+requestData.idGmmFrom);
        const snapshot_doc_gmm = await docRef_gmm.get();
        if (snapshot_doc_gmm===null||snapshot_doc_gmm.exists === false){
            console.error("### No existe el GMM");
            const response4:mdl.IGmmQuery_RemoteResponse = {
                idResult:err.GMM_NOT_EXISTS,
                text:'No existe GMM',
                size:0
            };
            return (response4);
        }
        const gmmDocumentData = snapshot_doc_gmm.data();
        if (gmmDocumentData===null||gmmDocumentData===undefined){
            console.error("### No existe el GMM");
            const response5:mdl.IGmmQuery_RemoteResponse = {
                idResult:err.FIRESTORE_READ_GENERAL_ERROR,
                text:'No existe GMM',
                size:0
            };
            return (response5);
        }
        const gmm:mdl.IGmm_BBDD = mdl.getGmmObject(gmmDocumentData);
        if (gmm.idUser !== uid){
            console.error("### El GMM no era del usuairo");
            const response6:mdl.IGmmQuery_RemoteResponse = {
                idResult:err.GMM_NOT_YOURS,
                text:'No existe GMM',
                size:0
            };
            return (response6);
        }
        //Ya tenemos el objeto de base de datos con el GMM a partir del cual realizaremos la búsqueda
        //Montamos la consulta
        const firestoreCollectionRef = admin.firestore().collection("/gmm");
        const geohashGmmFrom:string = gmm.geohash.substring(0,props.SIZE_GEOHASH_QUERY);
        const neighboursObj:geohash.Neighbours = geohash.neighbours(geohashGmmFrom);
        console.log("### Calculados vecinos e="+neighboursObj.e+" o="+neighboursObj.w+" n="+neighboursObj.n+" s="+neighboursObj.s);
        let queryGmmResultBefore;
        let queryGmmResultAfter;
        if (requestData.fromDateTimeMs===null||requestData.fromDateTimeMs===undefined){
            queryGmmResultBefore = await firestoreCollectionRef.
                where(props.COLUMN_GEOHASH_QUERY_GEOM,'==',geohashGmmFrom).
                where('gmmDateTime','<',gmm.gmmDateTime).
                orderBy('gmmDateTime','desc').
                limit(props.SIZE_QUERY_GEOM_GMM).
                get();
            queryGmmResultAfter = await firestoreCollectionRef.
                where(props.COLUMN_GEOHASH_QUERY_GEOM,'==',geohashGmmFrom).
                where('gmmDateTime','>',gmm.gmmDateTime).
                orderBy('gmmDateTime','asc').
                limit(props.SIZE_QUERY_GEOM_GMM).
                get();
        }else{
            queryGmmResultAfter = await firestoreCollectionRef.
                where(props.COLUMN_GEOHASH_QUERY_GEOM,'==',geohashGmmFrom).
                where('gmmDateTime','>',admin.firestore.Timestamp.fromMillis(requestData.fromDateTimeMs)).
                orderBy('gmmDateTime','asc').
                limit(requestData.currentCountAfter!==undefined
                    ?props.SIZE_QUERY_GEOM_GMM-requestData.currentCountAfter
                    :props.SIZE_QUERY_GEOM_GMM).
                get();
        }
        //Procesamos los datos para devolverlos(los anónimos hay que quitar información)
        const returnGmmArray:mdl.IGmm_ForQueries [] = processPrivateGmm(queryGmmResultAfter,uid);
        if (queryGmmResultBefore!==undefined){
            returnGmmArray.push(...processPrivateGmm(queryGmmResultBefore,uid));
        }
        console.info("### Devolviendo GMMS tamaño="+returnGmmArray.length);
        const responseOK:mdl.IGmmQuery_RemoteResponse = {
            idResult:1,
            text:'Consulta correcta',
            size:returnGmmArray.length,
            gmms:returnGmmArray
        };
        return (responseOK);
    } catch (error){
        console.error("### Error General:"+error);
        const response7:mdl.IGmmQuery_RemoteResponse = {
            idResult:err.FIRESTORE_GENERAL_ERROR,
            text:'Error general',
            size:0
        };
        return (response7);
    }
});

export const queryGmmRequestGeomTime = functions.https.onCall(async (data, context) => {
    const requestData:mdl.IGmmQuery_request = data;
    console.log("### queryGmmRequestGeomTime=> Recibidos los datos="+JSON.stringify(requestData));
    // Authentication / user information is automatically added to the request.
    if (context===null||context.auth===null||context.auth===undefined){
        console.error("### Usuario no EXISTE en bbdd");
        const response1:mdl.IGmmQuery_RemoteResponse = {
            idResult:err.USER_NOT_LOGGED,
            text:'Usuario no logado',
            size:0
        };
        return (response1);
    }
    const uid = context.auth.uid;
    //Comprobaciones para poder realizar el comentario en el GMM
    try{
        //Primero recuperamos el usuario
        const docRef_user = admin.firestore().doc('users/'+uid);
        const snapshot_doc_user = await docRef_user.get();
        if (snapshot_doc_user===null||snapshot_doc_user.exists === false){
          console.error("### Usuario no EXISTE en bbdd");
          const response2:mdl.IGmmQuery_RemoteResponse = {
            idResult:err.USER_NOT_EXISTS,
            text:'Usuario no existe',
            size:0
          };
          return (response2);
        }
        const userDocumentData = snapshot_doc_user.data();
        if (userDocumentData===null||userDocumentData===undefined){
          console.error("### Usuario no EXISTE en bbdd");
          const response3:mdl.IGmmQuery_RemoteResponse = {
            idResult:err.FIRESTORE_READ_GENERAL_ERROR,
            text:'Usuario no existe',
            size:0
          };
          return (response3);
        }
        //const userBBDD:mdl.IUser_BBDD=mdl.getUserObject(userDocumentData);
        //Ahora recuperamos el GMM
        const docRef_gmm = admin.firestore().doc('gmm/'+requestData.idGmmFrom);
        const snapshot_doc_gmm = await docRef_gmm.get();
        if (snapshot_doc_gmm===null||snapshot_doc_gmm.exists === false){
            console.error("### No existe el GMM");
            const response4:mdl.IGmmQuery_RemoteResponse = {
                idResult:err.GMM_NOT_EXISTS,
                text:'No existe GMM',
                size:0
            };
            return (response4);
        }
        const gmmDocumentData = snapshot_doc_gmm.data();
        if (gmmDocumentData===null||gmmDocumentData===undefined){
            console.error("### No existe el GMM");
            const response5:mdl.IGmmQuery_RemoteResponse = {
                idResult:err.FIRESTORE_READ_GENERAL_ERROR,
                text:'No existe GMM',
                size:0
            };
            return (response5);
        }
        const gmm:mdl.IGmm_BBDD = mdl.getGmmObject(gmmDocumentData);
        if (gmm.idUser !== uid){
            console.error("### El GMM no era del usuairo");
            const response6:mdl.IGmmQuery_RemoteResponse = {
                idResult:err.GMM_NOT_YOURS,
                text:'No existe GMM',
                size:0
            };
            return (response6);
        }
        //Ya tenemos el objeto de base de datos con el GMM a partir del cual realizaremos la búsqueda
        //Montamos la consulta
        const firestoreCollectionRef = admin.firestore().collection("/gmm");
        //#### Filtrado temporal #####
        const gmmDateFrom:Date = dateFns.subHours(gmm.gmmDateTime.toDate(),props.HOURS_QUERY_GMM_GEOM_TIME);
        const gmmDateTo:Date = dateFns.addHours(gmm.gmmDateTime.toDate(),props.HOURS_QUERY_GMM_GEOM_TIME);
        console.log("### Filtrado desde="+gmmDateFrom+" hasta="+gmmDateTo);
        //##### Filtrado espacial #####
        const geohashGmmFrom:string = gmm.geohash.substring(0,props.SIZE_GEOHASH_QUERY);
        const neighboursObj:geohash.Neighbours = geohash.neighbours(geohashGmmFrom);
        console.log("### Calculados vecinos de:"+geohashGmmFrom+
            " e="+neighboursObj.e+" o="+neighboursObj.w+" n="+neighboursObj.n+" s="+neighboursObj.s);
        let queryGmmResult;
        if (requestData.fromDateTimeMs===null||requestData.fromDateTimeMs===undefined)
            queryGmmResult = await firestoreCollectionRef.
                where(props.COLUMN_GEOHASH,'==',geohashGmmFrom).
                where('gmmDateTime','>',gmmDateFrom).
                where('gmmDateTime','<',gmmDateTo).
                orderBy('gmmDateTime','asc').
                limit(props.SIZE_QUERY_GMM).
                get();
        else
            queryGmmResult = await firestoreCollectionRef.
                where(props.COLUMN_GEOHASH,'==',geohashGmmFrom).
                where('gmmDateTime','<',gmmDateTo).
                where('gmmDateTime','>',admin.firestore.Timestamp.fromMillis(requestData.fromDateTimeMs)).
                orderBy('gmmDateTime','asc').
                limit(props.SIZE_QUERY_GMM).
                get();
        //Comprobamos tamaño de la consulta
        if (queryGmmResult.size===0){
            const responseOK_size0:mdl.IGmmQuery_RemoteResponse = {
                idResult:1,
                text:'Consulta correcta, sin resultados',
                size:0
            };
            return responseOK_size0;
        }
        //Procesamos los datos para devolverlos(los anónimos hay que quitar información)
        const returnGmmArray:mdl.IGmm_ForQueries [] = processPrivateGmm(queryGmmResult,uid);
        if (queryGmmResult.size===props.SIZE_QUERY_GMM){
            //Tenemos muchos GMM, tendremos que paginar.
        }
        console.info("### Devolviendo GMMS tamaño="+returnGmmArray.length);
        const responseOK:mdl.IGmmQuery_RemoteResponse = {
            idResult:1,
            text:'Consulta correcta',
            size:returnGmmArray.length,
            gmms:returnGmmArray
        };
        return (responseOK);
    } catch (error){
        console.error("### Error General:"+error);
        const response7:mdl.IGmmQuery_RemoteResponse = {
            idResult:err.FIRESTORE_GENERAL_ERROR,
            text:'Error general',
            size:0
        };
        return (response7);
    }
});

export const queryGmmOnlineTime = functions.https.onCall(async (data, context) => {
    console.log("### queryGmmOnlineTime ###");
    if (context===null||context.auth===null||context.auth===undefined){
        console.error("### Usuario no EXISTE en bbdd");
        const response1:mdl.IGmmQuery_RemoteResponse = {
            idResult:err.USER_NOT_LOGGED,
            text:'Usuario no logado',
            size:0
        };
        return (response1);
    }
    const uid = context.auth.uid;
    try{
        //Primero recuperamos el usuario
        const docRef_user = admin.firestore().doc('users/'+uid);
        const snapshot_doc_user = await docRef_user.get();
        if (snapshot_doc_user===null||snapshot_doc_user.exists === false){
          console.error("### Usuario no EXISTE en bbdd");
          const response2:mdl.IGmmQuery_RemoteResponse = {
            idResult:err.USER_NOT_EXISTS,
            text:'Usuario no existe',
            size:0
          };
          return (response2);
        }
        const userDocumentData = snapshot_doc_user.data();
        if (userDocumentData===null||userDocumentData===undefined){
          console.error("### Usuario no EXISTE en bbdd");
          const response3:mdl.IGmmQuery_RemoteResponse = {
            idResult:err.FIRESTORE_READ_GENERAL_ERROR,
            text:'Usuario no existe',
            size:0
          };
          return (response3);
        }
        //const userBBDD:mdl.IUser_BBDD=mdl.getUserObject(userDocumentData);
        //Comprobaciones relativas al usuario
        await docRef_user.update({ dateLastOnlineGmmRequest: admin.firestore.Timestamp.now() });
        //Montamos la consulta
        const firestoreCollectionRef = admin.firestore().collection("/gmm");
        const gmmDateFrom:Date = dateFns.subMinutes(new Date(),props.MINUTES_QUERY_ONLINE_GMM);
        const gmmDateTo:Date =  dateFns.addMinutes(new Date(),props.MINUTES_QUERY_ONLINE_GMM);
        //Comprobación de fecha correcta en el caso de pasarla como parémetro para paginación
        const queryGmmResult = await firestoreCollectionRef.
                where('gmmDateTime','>',gmmDateFrom).
                where('gmmDateTime','<',gmmDateTo).
                orderBy('gmmDateTime','desc').
                limit(props.SIZE_QUERY_GMM).
                get();
        //Procesamos los datos para devolverlos(los anónimos hay que quitar información)
        console.log("### Resultado consulta onLine size="+queryGmmResult.size);
        const returnGmmArray:mdl.IGmm_ForQueries [] = processPrivateGmm(queryGmmResult,uid);
        if (queryGmmResult.size===props.SIZE_QUERY_GMM){
            //Tenemos muchos GMM, tendremos que paginar.
            console.warn("### Se ha alcanzado el límite de GMM a descargar en consulta ON-Line por tiempo ###");
        }
        console.info("### Devolviendo GMMS tamaño="+returnGmmArray.length);
        const responseOK:mdl.IGmmQuery_RemoteResponse = {
            idResult:1,
            text:'Consulta correcta',
            size:returnGmmArray.length,
            gmms:returnGmmArray
        };
        return (responseOK);
    } catch (error){
        console.error("### Error General:"+error);
        const response4:mdl.IGmmQuery_RemoteResponse = {
            idResult:err.FIRESTORE_GENERAL_ERROR,
            text:'Error general',
            size:0
        };
        return (response4);
    }
});

/**
 * 
 * @param queryGmmResult 
 * @param idGmmFrom GMM de origen desde donde hacemos consulta
 * @param idUserFrom Identificador del usuario que hace la consulta
 */
function processPrivateGmm(
    queryGmmResult:FirebaseFirestore.QuerySnapshot,
    idUserFrom:string):mdl.IGmm_ForQueries []{
    //Procesamos los datos para devolverlos(los anónimos hay que quitar información)
    const returnGmmArray:mdl.IGmm_ForQueries [] = new Array<mdl.IGmm_ForQueries>();
    let contGmm = 0;
    queryGmmResult.forEach(snapshotGmm=>{
        const localGmm:mdl.IGmm_ForQueries = mdl.getGmmObjectForQueries(snapshotGmm.id,snapshotGmm.data());
        if (localGmm.isAnonimus){
            if (localGmm.textDesc!==null) localGmm.textDesc = undefined;
            localGmm.nick = undefined;
        }
        //No queremos devolver los gmm propios del usuario
        if (idUserFrom===undefined){
            //Eliminamos esta información, no la vamos a enviar
            localGmm.idUser = undefined;
            returnGmmArray[contGmm] = localGmm;
            contGmm++;
        }else{
            if (idUserFrom!==localGmm.idUser){
                //Eliminamos esta información, no la vamos a enviar
                localGmm.idUser = undefined;
                returnGmmArray[contGmm] = localGmm;
                contGmm++;
            }
        }
    })
    return returnGmmArray;
}