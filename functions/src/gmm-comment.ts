import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import * as mdl from './model';
import { GeoPosition } from 'geo-position.ts';
import * as props from './properties';
import * as err from './error-codes';
import { i18n } from './languages';
import { I18nResolver } from 'i18n-ts';


export const newGmmCmmRequest = functions.https.onCall(async (data, context) => {
    const requestData:mdl.IGmmComment_request = data;
    console.log("### newGmmCmmRequest=> Recibidos los datos="+JSON.stringify(requestData));
    // Authentication / user information is automatically added to the request.
    if (context===null||context.auth===null||context.auth===undefined){
      const response:mdl.IGmm_RemoteResponse = { idResult:err.USER_NOT_LOGGED,text:'### Usuario no logado en Firebase'} ;
      return (response);
    }
    const uid = context.auth.uid;
    //Comprobaciones para poder realizar el comentario en el GMM
    try{
      //Primero recuperamos el usuario
      const docRef_user = admin.firestore().doc('users/'+uid);
      const snapshot_doc_user = await docRef_user.get();
      if (snapshot_doc_user === null || snapshot_doc_user.exists === false){
        console.error("### Usuario no EXISTE en bbdd");
        const response:mdl.IGmm_RemoteResponse = {idResult:err.USER_NOT_EXISTS,text:'Usuario no existe'};
        return (response);
      }
      const userDocumentData = snapshot_doc_user.data();
      if (userDocumentData === undefined || userDocumentData === null){
        const response:mdl.IGmm_RemoteResponse = { idResult:err.FIRESTORE_READ_GENERAL_ERROR,text:'### No se ha podido recuperar el usuario'} ;
        return (response);
      }
      const userBBDD:mdl.IUser_BBDD=mdl.getUserObject(userDocumentData);
      //Ahora recuperamos el GMM
      const docRef_gmm = admin.firestore().doc('gmm/'+requestData.idGmm);
      const snapshot_doc_gmm = await docRef_gmm.get();
      if (snapshot_doc_gmm === null || snapshot_doc_gmm.exists === false){
        console.error("### No existe el GMM");
        const response:mdl.IGmm_RemoteResponse = {idResult:err.GMM_CMM_NOT_EXISTS,text:'No existe GMM'};
        return (response);
      }
      const gmmDocumentData = snapshot_doc_gmm.data();
      if (gmmDocumentData === null){
        console.error("### Recuperado GMM sin DATOS");
          const response:mdl.IGmm_RemoteResponse = { idResult:err.FIRESTORE_READ_GENERAL_ERROR,text:'### GMM sin datos'} ;
          return (response);
      }
      //Ya tenemos el GMM que queremos comentar.
      const gmm:mdl.IGmm_BBDD = mdl.getGmmObject(gmmDocumentData);
      //Ahora necesitamos el GMM origen desde donde vamos a realizar el comentario.
      const docRef_gmmFrom = admin.firestore().doc('gmm/'+requestData.idGmmFrom);
      const snapshot_doc_gmmFrom = await docRef_gmmFrom.get();
      if (snapshot_doc_gmmFrom === null || snapshot_doc_gmmFrom.exists === false){
        console.error("### No existe el GMM FROM");
        const response:mdl.IGmm_RemoteResponse = {idResult:err.GMM_NOT_EXISTS,text:'No existe GMM FROM'};
        return (response);
      }
      const gmmDocumentDataFrom = snapshot_doc_gmmFrom.data();
      if (gmmDocumentDataFrom === null){
        console.error("### Recuperado GMM FROM sin DATOS");
        const response:mdl.IGmm_RemoteResponse = { idResult:err.FIRESTORE_READ_GENERAL_ERROR,text:'### GMM FROM sin datos'} ;
        return (response);    
      }
      const gmmFrom:mdl.IGmm_BBDD = mdl.getGmmObject(gmmDocumentDataFrom);
      //Ya tenemos los 2 GMM
      //...el segundo GMM tiene que ser mío
      if (gmmFrom.idUser !== uid){
        console.error("### El GMM from no era mío!!!");
        const response:mdl.IGmm_RemoteResponse = {idResult:err.GMM_NOT_YOURS,text:'El GMM from no es tuyo!'};
        return (response);
      }
      const xPoint = new GeoPosition(gmm.geom.latitude, gmm.geom.longitude);
      const yPoint = new GeoPosition(gmmFrom.geom.latitude, gmmFrom.geom.longitude);
      const distanceMeters:number = xPoint.Distance(yPoint);
      const distanceMinutes:number = Math.abs(gmm.msDateTime-gmmFrom.msDateTime)/(1000*60);
      console.log("### Distancia en metros entre los GMM="+distanceMeters+" y en tiempo="+distanceMinutes+" minutos");
      if (distanceMeters>=props.METERS_MAX_GMM_COMMENT){
        if (distanceMinutes>=props.MINUTES_MAX_GMM_COMMENT){
          const response:mdl.IGmm_RemoteResponse = { idResult:err.GMM_CMM_DISTANCE_ERROR,text:'### DISTANCIA incorrecta'} ;
          return (response);
        } 
      }
      console.log("### La distacia para comentar es correcta="+distanceMeters);
      const docRef_gmmCmm:FirebaseFirestore.DocumentReference = 
        admin.firestore().collection('/gmm/'+requestData.idGmm+'/comments').doc();
      const newComment:mdl.IGmmComment_BBDD = mdl.newGmmCmmBBDD(
        requestData,
        userBBDD.nick,
        admin.firestore.Timestamp.now(),
        uid);
      const writeResult_cmm = await docRef_gmmCmm.set(newComment);
      if (writeResult_cmm === undefined || writeResult_cmm === null){
        const response:mdl.IGmm_RemoteResponse = { idResult:err.FIRESTORE_WRITE_GENERAL_ERROR,text:'### No ha sido posible el guardado'} ;
        return (response); 
      }
      //Hemos guardado correctamente el COMENTARIO en la colección del GMM.
      //Mandamos mensaje por mensajería 
      const docRef_userOwner = admin.firestore().doc('users/'+gmm.idUser);
      const snapshot_doc_userOwner = await docRef_userOwner.get();
      const userOwnerDocumentData = snapshot_doc_userOwner.data();
      if (userOwnerDocumentData!==undefined){
        const userOwnerBBDD:mdl.IUser_BBDD=mdl.getUserObject(userOwnerDocumentData);
        if (userOwnerBBDD.fcmToken!==undefined){
            sendMessageNewCMMToDevice(
              userOwnerBBDD.fcmToken,
              requestData.idGmm,
              userBBDD.nick,
              userOwnerBBDD.language);
        }else{
          console.error("### El usuario="+userOwnerBBDD.nick+" no tiene FCM Token");
        }
      }else{
        console.error("### No existia en bb.dd el usuario="+gmm.idUser);
      }
      //Actualizamos el contador de comentarios de otros GMMs.
      const increment = admin.firestore.FieldValue.increment(1);
      await docRef_user.update({ gmmOtherComments: increment });
      //Actualización del GMM origen
      if (requestData.likeLevel !== null && requestData.likeLevel !== undefined){
        if (requestData.likeLevel>0) await docRef_gmm.update({ likes: increment });
        if (requestData.likeLevel<0) await docRef_gmm.update({ dislikes: increment });
      }
      //Aumentamos el número de comentarios del GMM
      await docRef_gmm.update({ numComments: increment });
      //También tenemos que guardar el comentario en una colección del usuario
      const docRef_userCmm:FirebaseFirestore.DocumentReference = 
        admin.firestore().collection('/users/'+uid+'/commentsOtherGmm').doc(requestData.idGmm);
      newComment.idGmmComment = docRef_gmmCmm.id;
      const writeResult_userCmm = await docRef_userCmm.set(newComment);
      if (writeResult_userCmm === undefined || writeResult_userCmm === null){
        //Guardado incorrecto del comentario en el usuario.
        const response:mdl.IGmm_RemoteResponse = { idResult:err.FIRESTORE_WRITE_GENERAL_ERROR,text:'### No ha sido posible el guardado del comment en el usuario'} ;
        return (response); 
      }
      const responseOK:mdl.IGmm_RemoteResponse = { idResult:1,text:'### Creado comentario!!!',idGmm:docRef_gmmCmm.id} ;
      return (responseOK);
    } catch (error){
        console.error("### Error General:"+error);
        const response:mdl.IGmm_RemoteResponse = { idResult:err.FIRESTORE_GENERAL_ERROR,text:'### Error general en Firebase'} ;
        return (response);
    }
});

export const updateGmmCmmRequest = functions.https.onCall(async (data, context) => {
  const requestData:mdl.IGmmComment_request = data;
  console.log("### updateGmmCmmRequest=> Recibidos los datos="+JSON.stringify(requestData));
  // Authentication / user information is automatically added to the request.
  if (context===null||context.auth===null||context.auth===undefined){
    const response:mdl.IGmm_RemoteResponse = { idResult:err.USER_NOT_LOGGED,text:'### Usuario no logado en Firebase'} ;
    return (response);
  }
  // Comprobación de datos de entrada
  if (requestData.idGmmComment===null||requestData.idGmmComment===undefined){
    const response:mdl.IGmm_RemoteResponse = { idResult:err.FIRESTORE_READ_GENERAL_ERROR,text:'### Faltan datos de entrada idGmmCmm'} ;
    return (response);
  }
  const uid = context.auth.uid;
  //Comprobaciones para poder realizar el comentario en el GMM
  try{
    //Ahora recuperamos el Comentario
    const docRef_gmmCmm = admin.firestore().doc('/gmm/'+requestData.idGmm+'/comments/'+requestData.idGmmComment);
    const snapshot_doc_gmmCmm = await docRef_gmmCmm.get();
    if (snapshot_doc_gmmCmm === null || snapshot_doc_gmmCmm.exists === false){
      console.error("### No existe el GMMCmm");
      const response:mdl.IGmm_RemoteResponse = {idResult:err.GMM_NOT_EXISTS,text:'No existe GMMCmm'};
      return (response);
    }
    const gmmDocumentData = snapshot_doc_gmmCmm.data();
    if (gmmDocumentData === null){
      console.error("### Recuperado GMMCmm sin DATOS");
        const response:mdl.IGmm_RemoteResponse = { idResult:err.FIRESTORE_READ_GENERAL_ERROR,text:'### GMM sin datos'} ;
        return (response);
    }
    //Ya tenemos el comentario que queremos actualizar
    const gmmCmm:mdl.IGmmComment_BBDD = mdl.getGmmCmmObject(gmmDocumentData);
    //Comprovaciones varias
    if (gmmCmm.idUser!==uid){
      const response:mdl.IGmm_RemoteResponse = { idResult:err.GMM_CMM_NOT_YOURS,text:'### Intentando modificar comentario de otro usuario'} ;
      return (response);
    }
    //Actualizamos datos
    gmmCmm.textDesc = requestData.textDesc;
    gmmCmm.textDescLang = requestData.textDescLang;
    //Tendremos que recuperar el GMM original para actualizar datos si hemos cambiado el likeLevel
    if (gmmCmm.likeLevel !== requestData.likeLevel){
      //Nos tocará actualizar los datos de Gmm que hemos comentado
      //Ahora recuperamos el GMM
      const docRef_gmm = admin.firestore().doc('gmm/'+requestData.idGmm);
      const snapshot_doc_gmm = await docRef_gmm.get();
      if (snapshot_doc_gmm === null || snapshot_doc_gmm.exists === false){
        console.error("### No existe el GMM");
        const response:mdl.IGmm_RemoteResponse = {idResult:err.GMM_CMM_NOT_EXISTS,text:'No existe GMM'};
        return (response);
      }
      const gmmDocumentDataGmm = snapshot_doc_gmm.data();
      if (gmmDocumentDataGmm === null){
        console.error("### Recuperado GMM sin DATOS");
          const response:mdl.IGmm_RemoteResponse = { idResult:err.FIRESTORE_READ_GENERAL_ERROR,text:'### GMM sin datos'} ;
          return (response);
      }
      //Ya tenemos el GMM que queremos actualizar el likeLevel
      if (requestData.likeLevel===1&&gmmCmm.likeLevel===-1){
        const dec = admin.firestore.FieldValue.increment(-1);
        const inc = admin.firestore.FieldValue.increment(1);
        await docRef_gmm.update({ dislikes: dec, likes: inc });
      } else if (requestData.likeLevel===-1&&gmmCmm.likeLevel===1){
        const dec = admin.firestore.FieldValue.increment(-1);
        const inc = admin.firestore.FieldValue.increment(1);
        await docRef_gmm.update({ dislikes: inc, likes: dec });
      } else if (requestData.likeLevel===1&&gmmCmm.likeLevel===0){
        const inc = admin.firestore.FieldValue.increment(1);
        await docRef_gmm.update({ likes: inc });
      } else if (requestData.likeLevel===-1&&gmmCmm.likeLevel===0){
        const inc = admin.firestore.FieldValue.increment(1);
        await docRef_gmm.update({ dislikes: inc });
      } else if (requestData.likeLevel===0&&gmmCmm.likeLevel===1){
        const dec = admin.firestore.FieldValue.increment(-1);
        await docRef_gmm.update({ likes: dec });
      } else if (requestData.likeLevel===0&&gmmCmm.likeLevel===-1){
        const dec = admin.firestore.FieldValue.increment(-1);
        await docRef_gmm.update({ dislikes: dec });
      }
    }  
    gmmCmm.likeLevel = requestData.likeLevel;
    //También tenemos que guardar el comentario en una colección del usuario
    const docRef_userCmm:FirebaseFirestore.DocumentReference = 
      admin.firestore().collection('/users/'+uid+'/commentsOtherGmm').doc(requestData.idGmm);
    if (docRef_userCmm===null||docRef_userCmm===undefined){
      const response:mdl.IGmm_RemoteResponse = { idResult:err.FIRESTORE_UPDATING_GENERAL_ERROR,text:'### No ha sido posible actualizar el comentario del usuario'} ;
      return (response); 
    }
    //Update unitario
    const batch = admin.firestore().batch();
    batch.update(docRef_userCmm,gmmCmm);
    batch.update(docRef_gmmCmm,gmmCmm);
    const result = await batch.commit();
    if (result===null||result===undefined){
      const response:mdl.IGmm_RemoteResponse = { idResult:err.FIRESTORE_DELETE_GENERAL_ERROR,text:'### No ha sido posible borrar el comentario en el usuario'} ;
      return (response); 
    }    
    const responseOK:mdl.IGmm_RemoteResponse = { idResult:1,text:'### Comentarios actualizado !!!',idGmm:docRef_gmmCmm.id} ;
    return (responseOK);
  } catch (error){
      console.error("### Error General:"+error);
      const response:mdl.IGmm_RemoteResponse = { idResult:err.FIRESTORE_GENERAL_ERROR,text:'### Error general en Firebase'} ;
      return (response);
  }
});

export const deleteGmmCmmRequest = functions.https.onCall(async (data, context) => {
  const requestData:mdl.IGmmComment_request = data;
  console.log("### deleteGmmCmmRequest=> Recibidos los datos="+JSON.stringify(requestData));
  // Authentication / user information is automatically added to the request.
  if (context===null||context.auth===null||context.auth===undefined){
    const response:mdl.IGmm_RemoteResponse = { idResult:err.USER_NOT_LOGGED,text:'### Usuario no logado en Firebase'} ;
    return (response);
  }
  // Comprobación de datos de entrada
  if (requestData.idGmmComment===null||requestData.idGmmComment===undefined){
    const response:mdl.IGmm_RemoteResponse = { idResult:err.FIRESTORE_READ_GENERAL_ERROR,text:'### Faltan datos de entrada idGmmCmm'} ;
    return (response);
  }
  const uid = context.auth.uid;
  //Comprobaciones para poder realizar el comentario en el GMM
  try{
    //Ahora recuperamos el Comentario
    const docRef_gmmCmm = admin.firestore().doc('/gmm/'+requestData.idGmm+'/comments/'+requestData.idGmmComment);
    if (docRef_gmmCmm===null||docRef_gmmCmm===undefined){
      console.error("### No existe el GMMCmm");
      const response:mdl.IGmm_RemoteResponse = {idResult:err.GMM_CMM_NOT_EXISTS,text:'No existe GMMCmm'};
      return (response);
    }
    const snapshot_doc_gmmCmm = await docRef_gmmCmm.get();
    if (snapshot_doc_gmmCmm === null || snapshot_doc_gmmCmm.exists === false){
      console.error("### No existe el GMMCmm");
      const response:mdl.IGmm_RemoteResponse = {idResult:err.FIRESTORE_READ_GENERAL_ERROR,text:'No existe GMMCmm'};
      return (response);
    }
    const gmmCmmDocumentData = snapshot_doc_gmmCmm.data();
    if (gmmCmmDocumentData === null){
      console.error("### Recuperado GMMCmm sin DATOS");
        const response:mdl.IGmm_RemoteResponse = { idResult:err.FIRESTORE_READ_GENERAL_ERROR,text:'### GMM sin datos'} ;
        return (response);
    }
    //Ya tenemos el comentario que queremos actualizar
    const gmmCmm:mdl.IGmmComment_BBDD = mdl.getGmmCmmObject(gmmCmmDocumentData);
    //Comprovaciones varias
    if (gmmCmm.idUser!==uid){
      const response:mdl.IGmm_RemoteResponse = { idResult:err.GMM_CMM_NOT_YOURS,text:'### Intentando modificar comentario no propio'} ;
      return (response);
    }
    //Recuperamos el usuario para actualizar el número de comentarios de otros
    const docRef_user = admin.firestore().doc('users/'+uid);
    const snapshot_doc_user = await docRef_user.get();
    if (snapshot_doc_user === null || snapshot_doc_user.exists === false){
        console.error("### Usuario no EXISTE en bbdd");
        const response:mdl.IGmm_RemoteResponse = {idResult:err.USER_NOT_EXISTS,text:'Usuario no existe'};
        return (response);
      }
    //También tenemos que guardar el comentario en una colección del usuario
    const docRef_userCmm:FirebaseFirestore.DocumentReference = 
      admin.firestore().collection('/users/'+uid+'/commentsOtherGmm').doc(requestData.idGmm);
    if (docRef_userCmm===null||docRef_userCmm===undefined){
      const response:mdl.IGmm_RemoteResponse = { idResult:err.FIRESTORE_READ_GENERAL_ERROR,text:'### No ha sido posible recuperar el comentario en el usuario'} ;
      return (response); 
    }
    //Borrado unitario
    const batch = admin.firestore().batch();
    batch.delete(docRef_userCmm);
    batch.delete(docRef_gmmCmm);
    const result = await batch.commit();
    if (result===null||result===undefined){
      const response:mdl.IGmm_RemoteResponse = { idResult:err.FIRESTORE_DELETE_GENERAL_ERROR,text:'### No ha sido posible borrar el comentario en el usuario'} ;
      return (response); 
    }    
    //Incrementamos o decrementamos el número de likes
    const docRef_gmm:FirebaseFirestore.DocumentReference = admin.firestore().doc('/gmm/'+requestData.idGmm);
    if (docRef_gmm!==null){
      const decrement = admin.firestore.FieldValue.increment(-1);
      if (gmmCmm.likeLevel===-1){
        await docRef_gmm.update({ dislikes: decrement });
      }else if (gmmCmm.likeLevel===1){
        await docRef_gmm.update({ likes: decrement });
      }
      //Actualizamos número de comentarios del GMM
      await docRef_gmm.update({ numComments: decrement });
    }else{
      console.error("### No se ha encontrado el GMM="+requestData.idGmm+" en bb.dd");
    }
    //Actualizamos el contador de comentarios de otros GMMs.
    const increment = admin.firestore.FieldValue.increment(-1);
    await docRef_user.update({ gmmOtherComments: increment });
    const responseOK:mdl.IGmm_RemoteResponse = { idResult:1,text:'### Comentario eliminado !!!',idGmm:docRef_gmmCmm.id};
    return (responseOK);
  } catch (error){
      console.error("### Error General:"+error);
      const response:mdl.IGmm_RemoteResponse = { idResult:err.FIRESTORE_GENERAL_ERROR,text:'### Error general en Firebase'} ;
      return (response);
  }
});

export const newMaiCmmRequest = functions.https.onCall(async (data, context) => {
  const requestData:mdl.IGmmCmmMai_request = data;
  console.log("### newMaiCmmRequest=> Recibidos los datos="+JSON.stringify(requestData));
  // Authentication / user information is automatically added to the request.
  if (context===null||context.auth===null||context.auth===undefined){
    const response:mdl.IGmmCmmMai_response = { idResult:err.USER_NOT_LOGGED,text:'### Usuario no logado en Firebase'} ;
    return (response);
  }
  const uid = context.auth.uid;
  //Comprobaciones para poder realizar el MAI en ese comentario
  try{
    //Primero recuperamos el usuario
    const docRef_user = admin.firestore().doc('users/'+uid);
    const snapshot_doc_user = await docRef_user.get();
    if (snapshot_doc_user === null || snapshot_doc_user.exists === false){
      console.error("### Usuario no EXISTE en bbdd");
      const response:mdl.IGmmCmmMai_response = {idResult:err.USER_NOT_EXISTS,text:'Usuario no existe'};
      return (response);
    }
    //Comprobamos si el usuario ya ha hecho el MAI
    const docRef_userMaiCmm:FirebaseFirestore.DocumentReference = 
      admin.firestore().collection('/users/'+uid+'/maiOtherCmm').doc(requestData.idGmmComment);
    if (docRef_userMaiCmm!==null){
      const snapshot_doc_userMaiCmm = await docRef_userMaiCmm.get();
      if (snapshot_doc_userMaiCmm!==null&&snapshot_doc_userMaiCmm.exists){
        //Ojo, que el usuario ya había marcado este comentario como MAI
        const maiCmm:mdl.IMaiCmm_BBDD =  mdl.getMaiCmmObject(snapshot_doc_userMaiCmm);
        const response:mdl.IGmmCmmMai_response = {
          idResult:err.GMM_CMM_MAI_EXISTS,
          msDataTime: maiCmm.msInsertDateTime,
          text:'El usuario ya había marcado como inapropiado'};
        return (response);
      }
    }
    //A partir de aqui, el usuario no había registrado el MAI
    const maiCmmBBDD:mdl.IMaiCmm_BBDD = mdl.newMaiCmmBBDD(requestData.idGmm,admin.firestore.Timestamp.now());
    //Guardamos el documento
    await docRef_userMaiCmm.set(maiCmmBBDD);
    //Actualizamos contador de MAI del comentario
    const docRef_gmmCmm = admin.firestore().doc('/gmm/'+requestData.idGmm+'/comments/'+requestData.idGmmComment);
    const increment = admin.firestore.FieldValue.increment(1);
    await docRef_gmmCmm.update({ maiTimes: increment });
    //Y el contador de mai de comentarios del usuario
    await docRef_user.update({ maiOtherCmmCont: increment });
    const responseOK:mdl.IGmmCmmMai_response = { idResult:1,text:'### Creado MAI correctamente!!!'} ;
    return (responseOK);
  } catch (error){
      console.error("### Error General:"+error);
      const response:mdl.IGmmCmmMai_response = { idResult:err.FIRESTORE_GENERAL_ERROR,text:'### Error general en Firebase'} ;
      return (response);
  }
});

function sendMessageNewCMMToDevice(
  fcmToken:string,
  idGmm:string,
  nick:string,
  lang:string){
    const messages = new I18nResolver(i18n, lang).translation;
    const payload = {
      "notification":{
        "title":messages.NEW_COMMENT_TITLE,
        "body":messages.NEW_COMMENT_BODY_1+" "+nick+" "+messages.NEW_COMMENT_BODY_2
      },
      "data":{
        "idGmm":idGmm,
        "nick": nick,
        "type":"newCMMOfMyGMM"
      }
    };
    const options = {
      priority: "high",
      timeToLive: 60 * 60 * 24
    };
    admin.messaging().sendToDevice(fcmToken,payload,options).then(response=>{
      console.log("### Respuesta="+response);
    },(error)=>{
      console.error("### Error enviando mensaje:"+error);
    }).catch(error2=>{
      console.error("### Error2 enviando mensaje:"+error2);
    });

}