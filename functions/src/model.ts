import * as geohash from 'latlon-geohash';
import * as admin from 'firebase-admin';

export interface IUser_BBDD{
    nick: string;
    email:string;
    userType: number;
    language: string;
    dateRegistered: admin.firestore.Timestamp;
    dateLastAccess: admin.firestore.Timestamp;
    gmmLastNewDate?: admin.firestore.Timestamp;
    gmmHoursBetween: number;
    gmmOwnCont: number;
    gmmPreCont: number;
    gmmOtherComments:number;
    dateLastOnlineGmmRequest?: admin.firestore.Timestamp;
    maiOtherCmmCont?: number;
    maiOtherGmmCont?: number;
    fcmToken?:string;
    countryAdminList?:string[];
    gmmOnlineResolvedCont:number;
  }

  export interface IGmm_request {
    typeEmotion: number;
    levelEmotion: number;
    typeSocialEnvironment: number;
    longitud: number;
    latitud: number;
    spaceResolution:number;
    isAnonimus: boolean;
    textDesc?: string;
    textDescLang?: string;
    image64?: string;
    msDateTime:number;//Para los casos de subida de GMM en diferido.
  }
  
  export interface IGmm_ForQueries {
    idGmm: string;
    nick?: string;
    idUser?: string;
    typeEmotion: number;
    levelEmotion: number;
    typeSocialEnvironment: number;
    longitud: number;
    latitud: number;
    geom: admin.firestore.GeoPoint;
    geohash: string;
    spaceResolution:number;
    isAnonimus: boolean;
    textDesc?: string;
    textDescLang?: string;
    image64?: string;
    msDateTime: number;
    gmmType: number;
    timeResolution: number;
    isImage: boolean;    
    viewTimes: number;
    likes?: number;
    dislikes?: number;
    numComments?: number;
    maiTimes?: number;
  }

  export interface IGmm_Update_request{
    levelEmotion: number;
    isAnonimus: boolean;
    textDesc?: string;
    isImage: boolean;
  }

  export interface IGmm_BBDD extends IGmm_request {
    idUser: string;
    nick?: string;
    insertDateTime: admin.firestore.Timestamp;
    gmmDateTime: admin.firestore.Timestamp;
    geohash: string;
    geohash4: string; //39.1km 	× 	19.5km
    geohash5: string; //4.89km 	× 	4.89km
    geohash6: string; //1.22km 	× 	0.61km
    geom: admin.firestore.GeoPoint;
    timeResolution: number;
    viewTimes: number;
    idGmmPre?: string;
    gmmType: number;
    isImage: boolean;
    likes?: number;
    dislikes?: number;
    numComments?: number;    
    maiTimes?: number;
  }

  export interface IMaiCmm_BBDD {
    idGmm: string;
    insertDateTime: admin.firestore.Timestamp;
    msInsertDateTime: number;
  }

  export interface IMaiGmm_BBDD {
    insertDateTime: admin.firestore.Timestamp;
    msInsertDateTime: number;
  }
  
  export interface IUser_RemoteRequest{
    type:string;
    nick:string;
    language:string;
    fcmToken?:string;
  }

  export interface IUser_RemoteResponse{
    user?:IUser_BBDD;
    idResult:number;
  }

  export interface IGmm_RemoteRequest {
     type:string;
     gmm:IGmm_request; 
  }

  export interface IGmm_Update_RemoteRequest {
    idGmm:string;
    gmmUpdate:IGmm_Update_request; 
 }

  export interface IGmm_Delete_RemoteRequest {
    idGmm:string;
  }

  export interface IGmm_RemoteResponse {
    idResult:number;
    text:string;
    idGmm?:string;
  }

  export interface IGmmImage_RemoteResponse {
    idResult:number;
    text?:string;
    image64?:string;
  }

  //Modelo de datos para Comentarios
  export interface IGmmComment_request {
    idGmm: string;//Identificador del GMM donde se quiere comentar
    idGmmFrom: string; //Identificador del GMM origen del usuario.
    idGmmComment?: string; //Identificador del GMM para el update
    textDesc: string;
    textDescLang: string;
    likeLevel: number;
  }

  //Modelo de datos para marcar comentarios como no apropiados
  export interface IGmmCmmMai_request {
    idGmm: string; //Identificador del GMM origen del usuario.
    idGmmComment: string; //Identificador del GMM para el update
  }

  //Modelo de datos para marcar comentarios como no apropiados
  export interface IGmmCmmMai_response {
    idResult: number;
    msDataTime?: number;
    text:string;
  }

  //Modelo de datos para marcar GMM como no apropiados
  export interface IGmmMai_request {
    idGmm: string; //Identificador del GMM origen del usuario.
  }

  //Modelo de datos para marcar GMM como no apropiados
  export interface IGmmMai_response {
    idResult: number;
    msDataTime?: number;
    text:string;
  }


  //Modelo de datos para Queries
  export interface IGmmQuery_request {
    idGmmFrom?: string; //Identificador del GMM de referencia
    fromDateTimeMs?: number; //En caso de paginación, enviaremos a partir de cual, siempre en ms
    currentCountAfter?: number; //Es el número de GMM que tenemos actualmente en local.
  }

  export interface IGmmQuery_RemoteResponse {
    idResult:number;
    size:number;
    text?:string;
    gmms?:IGmm_ForQueries[];
  }

  export interface IGmmComment_BBDD {
    idGmmFrom: string;
    idUser: string;
    nick: string;
    gmmCommentDateTime: admin.firestore.Timestamp;
    msCommentDateTime?: number,
    textDesc: string;
    textDescLang: string;
    maiTimes?: number;
    likeLevel: number;    
    idGmmComment?: string;
  }

  export function newGmmCmmBBDD(
    data:IGmmComment_request,
    nick:string,
    gmmCommentDateTime: admin.firestore.Timestamp,
    idUser:string):IGmmComment_BBDD{
    
    const gmmCmm:IGmmComment_BBDD = {
      idGmmFrom: data.idGmmFrom,
      idUser: idUser,
      textDesc: data.textDesc,
      textDescLang: data.textDescLang,
      nick: nick,
      maiTimes: 0,
      likeLevel: data.likeLevel,
      gmmCommentDateTime: gmmCommentDateTime,
      msCommentDateTime: gmmCommentDateTime.toMillis()
    }
    return gmmCmm;
  }

  export function newMaiCmmBBDD(
    idGmm:string,
    insertDateTime: admin.firestore.Timestamp):IMaiCmm_BBDD{
    
    const maiCmm:IMaiCmm_BBDD = {
      idGmm: idGmm,
      insertDateTime: insertDateTime,
      msInsertDateTime: insertDateTime.toMillis()
    }
    return maiCmm;
  }

  export function newMaiGmmBBDD(
    insertDateTime: admin.firestore.Timestamp):IMaiGmm_BBDD{
    
    const maiGmm:IMaiGmm_BBDD = {
      insertDateTime: insertDateTime,
      msInsertDateTime: insertDateTime.toMillis()
    }
    return maiGmm;
  }

  //FUNCIONES
  export function newGmmBBDD(
        data:IGmm_request,
        idUser:string,
        nick:string,
        gmmType:number,
        timeResolution:number):IGmm_BBDD{
      
      const isImageLoad:boolean = (data.image64 !==null && data.image64!==undefined && data.image64.length>10) ? true : false;
      let gmmDateTime_:admin.firestore.Timestamp=admin.firestore.Timestamp.now();
      try{
        gmmDateTime_ = new admin.firestore.Timestamp(Math.trunc(data.msDateTime/1000),0);
      } catch (error){
        console.error("### Problemas convirtiendo fecha:"+data.msDateTime+" Error="+error);
      }
      const gmmBBDD:IGmm_BBDD = {
      isAnonimus: data.isAnonimus,
      nick: nick,
      geohash: geohash.encode(data.latitud,data.longitud,8),
      geohash4: geohash.encode(data.latitud,data.longitud,4),
      geohash5: geohash.encode(data.latitud,data.longitud,5),
      geohash6: geohash.encode(data.latitud,data.longitud,6),
      gmmDateTime: gmmDateTime_,
      msDateTime: data.msDateTime,
      gmmType: gmmType,
      idUser: idUser,
      insertDateTime: admin.firestore.Timestamp.now(),
      latitud: data.latitud,
      longitud: data.longitud,
      geom: new admin.firestore.GeoPoint(data.latitud, data.longitud),
      levelEmotion: data.levelEmotion,
      spaceResolution: data.spaceResolution,
      timeResolution: timeResolution,
      typeEmotion: data.typeEmotion,
      typeSocialEnvironment: data.typeSocialEnvironment,
      viewTimes: 0,
      maiTimes: 0,
      textDesc: data.textDesc,
      textDescLang: data.textDescLang,
      isImage: isImageLoad
    };
    return gmmBBDD;
  }

  export function getUserObject(userDocumentData:any):IUser_BBDD{
    let gmmOtherComments = 0;
    let maiOtherCmmCont = 0;
    let maiOtherGmmCont = 0;
    if (userDocumentData['gmmOtherComments']!==null&&userDocumentData['gmmOtherComments']!==undefined){
      gmmOtherComments = userDocumentData['gmmOtherComments'];
    }
    if (userDocumentData['maiOtherCmmCont']!==null&&userDocumentData['maiOtherCmmCont']!==undefined){
      maiOtherCmmCont = userDocumentData['maiOtherCmmCont'];
    }
    if (userDocumentData['maiOtherGmmCont']!==null&&userDocumentData['maiOtherGmmCont']!==undefined){
      maiOtherGmmCont = userDocumentData['maiOtherGmmCont'];
    }
    const userBBDD:IUser_BBDD={
      nick: userDocumentData['nick'],
      email:userDocumentData['email'],
      userType: userDocumentData['userType'],
      language: userDocumentData['language'],
      dateRegistered: userDocumentData['dateRegistered'],
      dateLastAccess: userDocumentData['dateLastAccess'],
      gmmOwnCont: userDocumentData['gmmOwnCont'],
      gmmPreCont: userDocumentData['gmmPreCont'],
      gmmLastNewDate: userDocumentData['gmmLastNewDate'],
      gmmHoursBetween: userDocumentData['gmmHoursBetween'],
      gmmOtherComments: gmmOtherComments,
      maiOtherCmmCont: maiOtherCmmCont,
      maiOtherGmmCont: maiOtherGmmCont,
      fcmToken: userDocumentData['fcmToken'],
      countryAdminList: userDocumentData['countryAdminList'],
      gmmOnlineResolvedCont: userDocumentData['gmmOnlineResolvedCont']
      };
    return userBBDD;
  }

  export function getGmmObject(gmmDocumentData:any):IGmm_BBDD{
    const gmmBBDD:IGmm_BBDD={
      idUser: gmmDocumentData['idUser'],
      nick: gmmDocumentData['nick'],
      gmmType: gmmDocumentData['gmmType'],
      typeEmotion: gmmDocumentData['typeEmotion'],
      typeSocialEnvironment:gmmDocumentData['typeSocialEnvironment'],
      spaceResolution: gmmDocumentData['spaceResolution'],
      timeResolution: gmmDocumentData['timeResolution'],
      levelEmotion: gmmDocumentData['levelEmotion'],
      gmmDateTime: gmmDocumentData['gmmDateTime'],
      msDateTime : gmmDocumentData['msDateTime'],
      insertDateTime: gmmDocumentData['insertDateTime'],
      latitud: gmmDocumentData['latitud'],
      longitud: gmmDocumentData['longitud'],
      geom: gmmDocumentData['geom'],
      geohash: gmmDocumentData['geohash'],
      geohash4: gmmDocumentData['geohash4'],
      geohash5: gmmDocumentData['geohash5'],
      geohash6: gmmDocumentData['geohash6'],
      textDesc: gmmDocumentData['textDesc'],
      textDescLang: gmmDocumentData['textDescLang'],
      isImage: gmmDocumentData['isImage'],
      viewTimes: gmmDocumentData['viewTimes'],
      isAnonimus: gmmDocumentData['isAnonimus'],
      likes: gmmDocumentData['likes'],
      dislikes: gmmDocumentData['dislikes'],
      maiTimes: gmmDocumentData['maiTimes'],
      };
    return gmmBBDD;
  }

  export function getGmmObjectForQueries(idGmm:string,gmmDocumentData:any):IGmm_ForQueries{
    const gmmBBDD:IGmm_ForQueries={
      idGmm: idGmm,
      idUser: gmmDocumentData['idUser'],
      nick: gmmDocumentData['nick'],
      gmmType: gmmDocumentData['gmmType'],
      typeEmotion: gmmDocumentData['typeEmotion'],
      typeSocialEnvironment:gmmDocumentData['typeSocialEnvironment'],
      spaceResolution: gmmDocumentData['spaceResolution'],
      timeResolution: gmmDocumentData['timeResolution'],
      levelEmotion: gmmDocumentData['levelEmotion'],
      msDateTime: gmmDocumentData['msDateTime'],
      latitud: gmmDocumentData['latitud'],
      longitud: gmmDocumentData['longitud'],
      geom: gmmDocumentData['geom'],
      geohash: gmmDocumentData['geohash'],
      textDesc: gmmDocumentData['textDesc'],
      textDescLang: gmmDocumentData['textDescLang'],
      isImage: gmmDocumentData['isImage'],
      viewTimes: gmmDocumentData['viewTimes'],
      isAnonimus: gmmDocumentData['isAnonimus'],
      likes: gmmDocumentData['likes'],
      dislikes: gmmDocumentData['dislikes'],
      numComments: gmmDocumentData['numComments'],
      maiTimes: gmmDocumentData['maiTimes']
      };
    return gmmBBDD;
  }

  export function getGmmCmmObject(gmmDocumentData:any):IGmmComment_BBDD{
    const gmmCmmBBDD:IGmmComment_BBDD={
      idGmmFrom: gmmDocumentData['idGmmFrom'],
      idUser: gmmDocumentData['idUser'],
      maiTimes: gmmDocumentData['maiTimes'],
      nick: gmmDocumentData['nick'],
      textDesc: gmmDocumentData['textDesc'],
      textDescLang: gmmDocumentData['textDescLang'],
      likeLevel: gmmDocumentData['likeLevel'],
      gmmCommentDateTime : gmmDocumentData['gmmCommentDateTime'],
      msCommentDateTime : gmmDocumentData['msCommentDateTime']
    };
    return gmmCmmBBDD;
  }

  export function getMaiCmmObject(gmmDocumentData:any):IMaiCmm_BBDD{
    const maiCmmBBDD:IMaiCmm_BBDD={
      idGmm: gmmDocumentData['idGmm'],
      insertDateTime: gmmDocumentData['insertDateTime'],
      msInsertDateTime: gmmDocumentData['msInsertDateTime']
    };
    return maiCmmBBDD;
  }

  export function getMaiGmmObject(gmmDocumentData:any):IMaiGmm_BBDD{
    const maiGmmBBDD:IMaiGmm_BBDD={
      insertDateTime: gmmDocumentData['insertDateTime'],
      msInsertDateTime: gmmDocumentData['msInsertDateTime']
    };
    return maiGmmBBDD;
  }

  

