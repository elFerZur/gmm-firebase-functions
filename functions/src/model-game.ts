import * as admin from 'firebase-admin';

export interface IGmmHist {
    idGmmHist: string;
    longitud: number;
    latitud: number;
    geom: admin.firestore.GeoPoint;
    spaceResolution:number;
    timeResolution: number;
    insertDateTime: admin.firestore.Timestamp;
    gmmDateTime: admin.firestore.Timestamp;
    geohash: string;
    geohash4: string; //39.1km 	× 	19.5km
    geohash5: string; //4.89km 	× 	4.89km
    geohash6: string; //1.22km 	× 	0.61km
    gmmHistType: number;
    idUser: string;
}