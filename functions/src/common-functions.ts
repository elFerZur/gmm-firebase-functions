
import * as admin from 'firebase-admin';

export async function deleteCollection(path:string) {
    try{
        const batch = admin.firestore().batch();
        const listDocuments = await admin.firestore().collection(path).listDocuments();
        listDocuments.map((documentReference) => {
            batch.delete(documentReference)
        });
        await batch.commit();
        return true;
    }catch (error){
        console.log("### Error borrando coleccion:"+error);
        return false;
    }
    
}