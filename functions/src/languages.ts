const es = {
    NEW_COMMENT_TITLE: "Nuevo comentario",
    NEW_COMMENT_BODY_1: "El usuario",
    NEW_COMMENT_BODY_2: "ha comentado tu GMM"
};
 
const en = {
    NEW_COMMENT_TITLE: "New comment",
    NEW_COMMENT_BODY_1: "The user",
    NEW_COMMENT_BODY_2: "has commented your GMM"
};
 
export const i18n = {
    en: en,
    es: es,
    default: en
};